class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{self.first_name} #{self.last_name}"
  end

  def enroll(course)
    raise "conflicting course" if self.courses.any? {|c| c.conflicts_with?(course)}
    self.courses << course unless self.courses.include?(course)
    course.students << self
  end

  def course_load
    course_credits = Hash.new(0)
    self.courses.each do |course|
      course_credits[course.department] += course.credits
    end
    course_credits
  end
end
